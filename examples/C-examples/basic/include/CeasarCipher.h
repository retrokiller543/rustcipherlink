#ifndef CEASAR_CIPHER_H
#define CEASAR_CIPHER_H


#ifdef __cplusplus
#include <string>
extern "C" {
#endif

#include <stdint.h>
#include <stddef.h>

// C-compatible struct
typedef struct CCeasar CCeasar;

// Function declarations
CCeasar* ceasar_new(uint32_t shift);
CCeasar* ceasar_new_with_rand_shift();
void ceasar_free(CCeasar* c_ceasar);

char* ceasar_encrypt(const CCeasar* c_ceasar, const char* input);
char* ceasar_decrypt(const CCeasar* c_ceasar, const char* input);
void ceasar_free_string(char* c_string);

#ifdef __cplusplus
} // extern "C"

// C++ class wrapper (optional, for C++ users)
class CeasarCipher {
public:
    explicit CeasarCipher(uint32_t shift) {
        this->cipher = ceasar_new(shift);
    }

    CeasarCipher() {
        this->cipher = ceasar_new_with_rand_shift();
    }

    ~CeasarCipher() {
        if (this->cipher != nullptr) {
            ceasar_free(this->cipher);
        }
    }

    std::string encrypt(const std::string &input) {
        char* result = ceasar_encrypt(this->cipher, input.c_str());
        std::string encrypted(result);
        ceasar_free_string(result);
        return encrypted;
    }

    std::string decrypt(const std::string &input) {
        char* result = ceasar_decrypt(this->cipher, input.c_str());
        std::string decrypted(result);
        ceasar_free_string(result);
        return decrypted;
    }

private:
    CCeasar* cipher;
};
#endif // __cplusplus

#endif // CEASAR_CIPHER_H
