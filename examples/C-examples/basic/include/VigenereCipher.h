#ifndef VIGENERE_CIPHER_H
#define VIGENERE_CIPHER_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stddef.h>

// Forward declaration of the C-compatible struct
typedef struct CVigenere CVigenere;

// Function to create a new Vigenere cipher instance
// Takes a C string as the key
CVigenere* vigenere_new(const char* key);

// Function to free a Vigenere cipher instance
void vigenere_free(CVigenere* c_vigenere);

// Function to encrypt a string using the Vigenere cipher
// Returns a newly allocated C string that should be freed after use
char* vigenere_encrypt(const CVigenere* c_vigenere, const char* input);

// Function to decrypt a string using the Vigenere cipher
// Returns a newly allocated C string that should be freed after use
char* vigenere_decrypt(const CVigenere* c_vigenere, const char* input);

// Function to free strings returned from Rust
void vigenere_free_string(char* c_string);

#ifdef __cplusplus
}
#endif

#ifdef __cplusplus
// C++ wrapper class (optional, for C++ users)
#include <string>

class VigenereCipher {
public:
    explicit VigenereCipher(const std::string& key) {
        this->cipher = vigenere_new(key.c_str());
    }

    explicit VigenereCipher(const char* key) {
        this->cipher = vigenere_new(key);
    }

    ~VigenereCipher() {
        if (this->cipher != nullptr) {
            vigenere_free(this->cipher);
        }
    }

    std::string encrypt(const std::string& input) {
        char* result = vigenere_encrypt(this->cipher, input.c_str());
        std::string encrypted(result);
        vigenere_free_string(result);
        return encrypted;
    }

    std::string decrypt(const std::string& input) {
        char* result = vigenere_decrypt(this->cipher, input.c_str());
        std::string decrypted(result);
        vigenere_free_string(result);
        return decrypted;
    }

private:
    CVigenere* cipher;
};
#endif // __cplusplus

#endif // VIGENERE_CIPHER_H
