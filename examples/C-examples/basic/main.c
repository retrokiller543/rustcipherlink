#include "include/RustCipherLink.h"
#include <stdlib.h>
#include <stdio.h>

int main() {
    // test 18
    CCeasar* ceasar = ceasar_new(3);
    if (!ceasar) {
        printf("Failed to create ceasar.\n");
        return 1;
    }
    char* encrypted = ceasar_encrypt(ceasar, "this is a super secret message that no one must get there hands on! Hello World!");

    if (encrypted) {
        printf("Encrypted: %s\n", encrypted);
        char* decrypted = ceasar_decrypt(ceasar, encrypted);
        if (decrypted) {
            printf("Decrypted: %s\n", decrypted);
            ceasar_free_string(decrypted);
        } else {
            printf("Decryption failed.\n");
        }
        ceasar_free_string(encrypted);
    } else {
        printf("Encryption failed.\n");
    }

    ceasar_free(ceasar);
    return 0;
}
