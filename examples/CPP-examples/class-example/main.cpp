#include <benchmark/benchmark.h>
#include "include/RustCipherLink.h"
#include <string>

static void BM_CeasarCipher_Encrypt(benchmark::State& state) {
    CeasarCipher cipher(3); // Shift of 3
    std::string long_input(10000, 'A'); // String of length 10000

    for (auto _ : state) {
        std::string encrypted = cipher.encrypt(long_input);
    }
}
BENCHMARK(BM_CeasarCipher_Encrypt);

static void BM_CeasarCipher_Decrypt(benchmark::State& state) {
    CeasarCipher cipher(3); // Shift of 3
    std::string long_input(10000, 'A'); // String of length 10000
    std::string encrypted = cipher.encrypt(long_input);

    for (auto _ : state) {
        std::string decrypted = cipher.decrypt(encrypted);
    }
}
BENCHMARK(BM_CeasarCipher_Decrypt);

static void BM_VigenereCipher_Encrypt(benchmark::State& state) {
    VigenereCipher cipher("SomeLongKey");
    std::string long_input(10000, 'A'); // String of length 10000

    for (auto _ : state) {
        std::string encrypted = cipher.encrypt(long_input);
    }
}
BENCHMARK(BM_VigenereCipher_Encrypt);

static void BM_VigenereCipher_Decrypt(benchmark::State& state) {
    VigenereCipher cipher("SomeLongKey");
    std::string long_input(10000, 'A'); // String of length 10000
    std::string encrypted = cipher.encrypt(long_input);

    for (auto _ : state) {
        std::string decrypted = cipher.decrypt(encrypted);
    }
}
BENCHMARK(BM_VigenereCipher_Decrypt);

BENCHMARK_MAIN();

/*#include "include/RustCipherLink.h"
#include <iostream>

int main() {

    CeasarCipher cipher(30); // Shift of 3
    std::string encrypted = cipher.encrypt("super secret key to be encrypted");
    std::string decrypted = cipher.decrypt(encrypted);

    std::cout << "Encrypted key: " << encrypted << std::endl;
    std::cout << "Decrypted key: " << decrypted << std::endl;


    {
        VigenereCipher lCipher("hej");
        std::string lEncrypted = lCipher.encrypt("hello, world!");
        std::string lDecrypted = lCipher.decrypt(lEncrypted);

        std::cout << "Encrypted: " << lEncrypted << std::endl;
        std::cout << "Decrypted: " << lDecrypted << std::endl;
    }

    // encrypt a string and then use that encrypted string as the key for a vigener cipher
    {
        VigenereCipher lCipher(encrypted);
        std::string lEncrypted = lCipher.encrypt("hello, world!");
        std::string lDecrypted = lCipher.decrypt(lEncrypted);

        std::cout << "Encrypted: " << lEncrypted << std::endl;
        std::cout << "Decrypted: " << lDecrypted << std::endl;
    }

    return 0;
}*/