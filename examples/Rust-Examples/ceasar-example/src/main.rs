use RustCipherLink::prelude::*;
use anyhow::Result;

fn main() -> Result<()> {
    // create a new instance of the struct with a specified shift
    let ceasar = Ceasar::new(3);
    let plain_text = "Hello World".to_string();
    let cipher_text = match ceasar.encrypt(plain_text) {
        Ok(txt) => txt,
        Err(e) => panic!("{}", e)
    }; // this will return a Error if we get any issues during encoding or decoding the characters
    println!("Cipher Text: {}", cipher_text);
    let decrypted_text = ceasar.decrypt(cipher_text)?; // this will return a Error if we get any issues during encoding or decoding the characters
    println!("Decrypted Text: {}", decrypted_text);

    // if needed we can encode Strings/&str to a Vec<usize> and then also decode that Vec back to a String
    let test = "hello world of encoded strings!".encode_str()?;
    println!("{:?}", test);
    let test_decoded = test.decode_str()?;
    println!("{}", &test_decoded);

    // if we encode using utf8 and use a shift that is large we run into the risk of getting invalid utf8 chars and therefore wont be easy to display
    let ceasar_high_shift = Ceasar::new(531600);
    let encrypted = ceasar_high_shift.encrypt(test_decoded)?;
    println!("{}", &encrypted);
    let encoded_text = encrypted.encode_str()?;
    println!("{:?}", &encoded_text[0]);
    println!("{:?}", &encoded_text[0] - ceasar_high_shift.shift); // but we can still simply do our decryption and it works just fine

    Ok(())
}
