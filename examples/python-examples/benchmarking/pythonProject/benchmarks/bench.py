from RustCipherLink import Ceasar


class TimeSuite:
    def __init__(self):
        self.text_to_decrypt = None
        self.cipher = None
        self.text_to_encrypt = None

    def setup(self):
        self.cipher = Ceasar(3)  # Example shift value
        self.text_to_encrypt = "Example text to encrypt"
        self.text_to_decrypt = "KHOOR#ZRUOG"

    def time_enrypt_ceasar(self):
        self.cipher.encrypt(self.text_to_encrypt)

    def time_decrypt_ceasar(self):
        self.cipher.decrypt(self.text_to_decrypt)