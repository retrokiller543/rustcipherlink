pub trait Encrypt {
    type Input;
    type Output;

    fn encrypt(&self, input: Self::Input) -> Self::Output;
}

pub trait Decrypt {
    type Input;
    type Output;

    fn decrypt(&self, input: Self::Input) -> Self::Output;
}

pub trait Encode {
    type Output;

    fn encode_str(&self) -> Self::Output;
}

pub trait Decode {
    type Output;

    fn decode_str(&self) -> Self::Output;
    fn flatten(&self) -> String;
}