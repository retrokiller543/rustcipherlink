mod encoders;

use crate::traits::{Decode, Encode};
use anyhow::Result;
#[cfg(any(feature = "sve", feature = "eng", feature = "sve-special"))]
use crate::utils::encoders::{decode_alphabet as decode_fn, encode_alphabet as encode_fn};
#[cfg(all(not(feature = "utf8"), any(feature = "sve", feature = "eng", feature = "sve-special")))]
use crate::utils::encoders::{decode_alphabet, encode_alphabet};
#[cfg(all(feature = "utf8", not(any(feature = "sve", feature = "eng", feature = "sve-special"))))]
use crate::utils::encoders::{decode_utf8 as decode_fn, encode_utf8 as encode_fn};


impl Encode for String {
    type Output = Result<Vec<usize>>;

    fn encode_str(&self) -> Self::Output {
        Ok(encode_fn(self)?)
    }
}

impl Encode for &str {
    type Output = Result<Vec<usize>>;

    fn encode_str(&self) -> Self::Output {
        Ok(encode_fn(self)?)
    }
}

impl Decode for Vec<usize> {
    type Output = Result<String>;

    fn decode_str(&self) -> Self::Output {
        Ok(decode_fn(self)?)
    }

    fn flatten(&self) -> String {
        todo!()
    }
}