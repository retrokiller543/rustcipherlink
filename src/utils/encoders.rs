use anyhow::Result;
#[cfg(all(feature = "parallel", not(feature = "sequential")))]
use crate::consts::{THRESHOLD};
#[cfg(any(feature = "sve", feature = "eng", feature = "sve-special"))]
use crate::consts::{ALPHABET, ALPHABET_LEN};
#[cfg(all(feature = "parallel", not(feature = "sequential")))]
use rayon::prelude::*;
use crate::error::CharacterError;

#[cfg(all(feature = "utf8", not(any(feature = "sve", feature = "eng", feature = "sve-special"))))]
pub(crate) fn encode_utf8(input: &str) -> Result<Vec<usize>> {
    let mut encoded = Vec::new();
    for c in input.chars() {
        let code_point = c as usize;
        encoded.push(code_point);
    }
    Ok(encoded)
}

#[cfg(any(feature = "sve", feature = "eng", feature = "sve-special"))]
pub(crate) fn encode_alphabet(input: &str) -> Result<Vec<usize>> {
    let data = input.to_lowercase();
    let mut output = Vec::new();

    #[cfg(all(feature = "parallel", not(feature = "sequential")))]
    {
        let data_len = data.chars().count();
        let encoded = if data_len > THRESHOLD {
            data.par_chars()
                .map(|x| {
                    encode_char(x)
                })
                .collect::<Result<Vec<_>, _>>()?
        } else {
            data.chars()
                .map(|x| {
                    encode_char(x)
                })
                .collect::<Result<Vec<_>, _>>()?
        };

        output.extend(encoded);
    }

    #[cfg(all(feature = "sequential", not(feature = "parallel")))]
    {
        let encoded = data.chars()
                .map(|x| {
                    encode_char(x)
                })
                .collect::<Result<Vec<_>, _>>()?;

        output = encoded;
    }

    Ok(output)
}

#[cfg(any(feature = "sve", feature = "eng", feature = "sve-special"))]
fn encode_char(input: char) -> Result<usize> {
    #[cfg(feature = "debug")]
    {
        let position = crate::prelude::ALPHABET.chars().position(|y| y == x);

        dbg!(&x, &position); // Log each character and its index (sequential branch)

        position.ok_or(CharacterParseError::InvalidCharacter.into())
    }
    #[cfg(not(feature = "debug"))]
    {
        ALPHABET
            .chars()
            .position(|y| y == input)
            .ok_or(CharacterError::InvalidCharacter(input).into())
    }
}

#[cfg(all(feature = "utf8", not(any(feature = "sve", feature = "eng", feature = "sve-special"))))]
pub(crate) fn decode_utf8(input: &[usize]) -> Result<String> {
    Ok(input.iter().map(|&x| {
        std::char::from_u32(x as u32).unwrap_or('\u{FFFD}') // Use Unicode Replacement Character for invalid values
    }).collect::<String>())
}

#[cfg(any(feature = "sve", feature = "eng", feature = "sve-special"))]
pub(crate) fn decode_alphabet(input: &[usize]) -> Result<String> {
    let mut output = String::new();

    #[cfg(all(feature = "parallel", not(feature = "sequential")))]
    {
        let input_len = input.len();
        let decoded = if input_len > THRESHOLD {
            input.par_iter()
                .map(|&x| {
                    decode_char(x)
                })
                .collect::<Result<String, _>>()?
        } else {
            input.iter()
                .map(|&x| {
                    decode_char(x)
                })
                .collect::<Result<String, _>>()?
        };

        output += &decoded;
    }

    #[cfg(all(feature = "sequential", not(feature = "parallel")))]
    {
        let decoded = input.iter()
                .map(|&x| {
                    decode_char(x)
                })
                .collect::<Result<String, _>>()?;

        output = decoded;
    }

    Ok(output)
}

#[cfg(any(feature = "sve", feature = "eng", feature = "sve-special"))]
fn decode_char(input: usize) -> Result<char> {
    ALPHABET
        .chars()
        .nth(input)
        .ok_or(CharacterError::InvalidIndex(input).into())
}