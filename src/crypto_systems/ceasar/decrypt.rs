use crate::crypto_systems::ceasar::Ceasar;
use crate::traits::{Decode, Decrypt, Encode};
#[cfg(all(feature = "parallel", not(feature = "sequential")))]
use rayon::prelude::*;
use crate::consts::ALPHABET_LEN;
use crate::crypto_systems::ceasar::error::CeasarError;

#[cfg_attr(feature = "python-integration", pyo3_helper_macros::py3_bind)]
impl Decrypt for Ceasar {
    type Input = String;
    type Output = Result<String, CeasarError>;

    fn decrypt(&self, cipher_text: Self::Input) -> <Ceasar as Decrypt>::Output {
        #[cfg(feature = "parallel")]
        #[cfg(not(feature = "sequential"))]
        #[cfg(not(feature = "c-bindings"))]
        return Ok(cipher_text
            //.to_lowercase()
            .encode_str()?
            .par_iter()
            .map(|&x| (x + ALPHABET_LEN - self.shift) % ALPHABET_LEN)
            .collect::<Vec<usize>>()
            .decode_str()?
            .to_lowercase());

        // fallback
        #[cfg(all(feature = "sequential", feature = "parallel"))]
        return Ok(cipher_text
            //.to_lowercase()
            .encode_str()?
            .iter()
            .map(|&x| (x + ALPHABET_LEN - self.shift) % ALPHABET_LEN)
            .collect::<Vec<usize>>()
            .decode_str()?
            .to_lowercase());
    }
}