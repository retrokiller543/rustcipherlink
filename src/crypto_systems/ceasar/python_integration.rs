use pyo3::PyErr;
use crate::crypto_systems::ceasar::error::CeasarError;

impl From<CeasarError> for PyErr {
    fn from(err: CeasarError) -> PyErr {
        match err {
            CeasarError::InvalidKey(key) => PyErr::new::<pyo3::exceptions::PyValueError, _>(format!("Invalid key: {}", key)),
            CeasarError::Error(err) => PyErr::new::<pyo3::exceptions::PyException, _>(format!("{}", err)),
        }
    }
}