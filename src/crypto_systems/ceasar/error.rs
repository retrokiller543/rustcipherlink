use thiserror::Error;

#[derive(Debug, Error)]
pub enum CeasarError {
    #[error("Invalid key {0}")]
    InvalidKey(usize),
    #[error("Error occurd: {0}")]
    Error(anyhow::Error),
}

impl From<anyhow::Error> for CeasarError {
    fn from(err: anyhow::Error) -> CeasarError {
        CeasarError::Error(err)
    }
}