use crate::crypto_systems::ceasar::Ceasar;
use crate::traits::Encrypt;

#[test]
fn test() {
    #[cfg(feature = "utf8")]
    let input = "Hello World".to_string();
    #[cfg(not(feature = "utf8"))]
    let input = "HelloWorld".to_string();
    let ceasar = Ceasar::new(3);
    let encrypted = ceasar.encrypt(input).unwrap();
    #[cfg(feature = "utf8")]
    assert_eq!(encrypted, "KHOOR#ZRUOG");
    #[cfg(not(feature = "utf8"))]
    assert_eq!(encrypted, "KHOORZRUOG");
}