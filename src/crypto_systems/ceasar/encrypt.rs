use crate::crypto_systems::ceasar::Ceasar;
use crate::traits::{Decode, Encode, Encrypt};
use crate::consts::{ALPHABET_LEN, THRESHOLD};
#[cfg(all(feature = "parallel", not(feature = "sequential")))]
use rayon::prelude::*;
use crate::crypto_systems::ceasar::error::CeasarError;

#[cfg_attr(feature = "python-integration", pyo3_helper_macros::py3_bind)]
impl Encrypt for Ceasar {
    type Input = String;
    type Output = Result<String, CeasarError>;

    fn encrypt(&self, input: Self::Input) -> <Ceasar as Encrypt>::Output {
        #[cfg(feature = "parallel")]
        #[cfg(not(feature = "sequential"))]
        #[cfg(not(feature = "c-bindings"))]
        {
            return if input.chars().count() < THRESHOLD {
                Ok(input
                    .to_lowercase()
                    .encode_str()?
                    .iter()
                    .map(|&x| (x + self.shift) % ALPHABET_LEN)
                    .collect::<Vec<usize>>()
                    .decode_str()?
                    .to_uppercase())
            } else {
                Ok(input
                    .to_lowercase()
                    .encode_str()?
                    .par_iter()
                    .map(|&x| (x + self.shift) % ALPHABET_LEN)
                    .collect::<Vec<usize>>()
                    .decode_str()?
                    .to_uppercase())
            }
        }

        // fallback if neither parallel nor sequential is enabled
        #[cfg(all(feature = "sequential", feature = "parallel"))]
        Ok(input
            .to_lowercase()
            .encode_str()?
            .iter()
            .map(|&x| (x + self.shift) % ALPHABET_LEN)
            .collect::<Vec<usize>>()
            .decode_str()?
            .to_uppercase())
    }
}