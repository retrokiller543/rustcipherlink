use super::Ceasar;
use std::os::raw::c_uint;
use std::ffi::{CString, CStr};
use std::os::raw::c_char;
use std::ptr;
use crate::traits::{Encrypt, Decrypt};

// C-compatible struct
#[repr(C)]
pub struct CCeasar {
    shift: c_uint,
}

// Conversion from Rust to C-compatible struct
impl From<Ceasar> for CCeasar {
    fn from(c: Ceasar) -> Self {
        CCeasar { shift: c.shift as c_uint }
    }
}

// Extern C functions
#[no_mangle]
pub extern "C" fn ceasar_new(shift: c_uint) -> *mut CCeasar {
    Box::into_raw(Box::new(CCeasar::from(Ceasar::new(shift as usize))))
}

#[no_mangle]
pub extern "C" fn ceasar_new_with_rand_shift() -> *mut CCeasar {
    Box::into_raw(Box::new(CCeasar::from(Ceasar::new_with_rand_shift())))
}

#[no_mangle]
pub extern "C" fn ceasar_free(c_ceasar: *mut CCeasar) {
    if !c_ceasar.is_null() {
        unsafe { let _ = Box::from_raw(c_ceasar); }
    }
}

#[no_mangle]
pub extern "C" fn ceasar_encrypt(c_ceasar: *const Ceasar, input: *const c_char) -> *mut c_char {
    if c_ceasar.is_null() || input.is_null() {
        return ptr::null_mut();
    }

    let c_ceasar = unsafe { &*c_ceasar };
    let cstr_input = unsafe { CStr::from_ptr(input) };

    match cstr_input.to_str() {
        Ok(str_input) => match c_ceasar.encrypt(str_input.to_string()) {
            Ok(encrypted) => {
                let c_string = CString::new(encrypted).expect("CString::new failed");
                c_string.into_raw()
            },
            Err(_) => ptr::null_mut() // Handle encryption error
        },
        Err(_) => ptr::null_mut() // Handle CStr to_str conversion error
    }
}

#[no_mangle]
pub extern "C" fn ceasar_decrypt(c_ceasar: *const Ceasar, input: *const c_char) -> *mut c_char {
    if c_ceasar.is_null() || input.is_null() {
        return ptr::null_mut();
    }

    let c_ceasar = unsafe { &*c_ceasar };
    let cstr_input = unsafe { CStr::from_ptr(input) };

    match cstr_input.to_str() {
        Ok(str_input) => match c_ceasar.decrypt(str_input.to_string()) {
            Ok(decrypted) => {
                let c_string = CString::new(decrypted).expect("CString::new failed");
                c_string.into_raw()
            },
            Err(_) => ptr::null_mut() // Handle decryption error
        },
        Err(_) => ptr::null_mut() // Handle CStr to_str conversion error
    }
}

// Additional function to free strings returned from Rust
#[no_mangle]
pub extern "C" fn ceasar_free_string(c_string: *mut c_char) {
    if !c_string.is_null() {
        unsafe { let _ = CString::from_raw(c_string); };
    }
}