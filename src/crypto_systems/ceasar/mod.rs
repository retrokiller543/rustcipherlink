use rand::{Rng, thread_rng};
use crate::consts::ALPHABET_LEN;

mod error;

/// Represents a Caesar cipher.
///
/// A Caesar cipher is a type of substitution cipher in which each letter in the plaintext is 'shifted' a certain number of places down the alphabet.
/// For example, with a shift of 1, A would be replaced by B, B would become C, and so on.
///
/// # Fields
///
/// * `shift` - A usize that represents the shift value used for encryption and decryption.
#[derive(Default, Debug, Clone, PartialEq, Eq)]
#[cfg_attr(feature = "python-integration", pyo3::pyclass)]
pub struct Ceasar {
    pub shift: usize
}

#[cfg_attr(feature = "python-integration", pyo3_helper_macros::py3_bind_pub)]
impl Ceasar {
    /// Creates a new Caesar cipher with the given shift value.
    ///
    /// # Arguments
    ///
    /// * `shift` - A usize that represents the shift value.
    ///
    /// # Returns
    ///
    /// * A new Caesar cipher with the specified shift value.
    ///
    /// # Example
    ///
    /// ```
    /// # use RustCipherLink::crypto_systems::ceasar::Ceasar;
    /// let ceasar = Ceasar::new(3);
    /// ```
    pub fn new(shift: usize) -> Self {
        Self { shift }
    }

    /// Creates a new Caesar cipher with a random shift value between 1 and 25.
    ///
    /// # Returns
    ///
    /// * A new Caesar cipher with a random shift value.
    ///
    /// # Example
    ///
    /// ```
    /// # use RustCipherLink::crypto_systems::ceasar::Ceasar;
    ///
    /// let ceasar = Ceasar::new_with_rand_shift();
    /// println!("Random shift: {}", ceasar.shift);
    /// ```
    pub fn new_with_rand_shift() -> Self {
        // select random shift
        let shift = thread_rng().gen_range(1..ALPHABET_LEN);
        Self { shift }
    }
}

pub mod encrypt;
pub mod decrypt;
mod tests;
#[cfg(feature = "python-integration")]
pub mod python_integration;
#[cfg(feature = "c-bindings")]
mod c_bindings;