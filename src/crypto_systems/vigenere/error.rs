use thiserror::Error;

#[derive(Debug, Error)]
pub enum VigenereError {
    #[error("Invalid key")]
    InvalidKey,
    #[error("Error occurd")]
    Error(anyhow::Error),
}

impl From<anyhow::Error> for VigenereError {
    fn from(err: anyhow::Error) -> VigenereError {
        VigenereError::Error(err)
    }
}