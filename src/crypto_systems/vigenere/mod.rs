use crate::crypto_systems::vigenere::error::VigenereError;
use crate::traits::Encode;

pub mod encrypt;
pub mod error;


#[cfg_attr(feature = "python-integration", pyo3::pyclass)]
pub struct Vigenere {
    key: String
}

#[cfg_attr(feature = "python-integration", pyo3_helper_macros::py3_bind_pub)]
impl Vigenere {
    pub fn new(key: String) -> Self{
        Self { key }
    }
}

#[cfg(feature = "c-bindings")]
mod c_bindings;
mod tests;
mod decrypt;