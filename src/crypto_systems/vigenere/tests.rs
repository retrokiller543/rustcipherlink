use super::*;
use crate::crypto_systems::vigenere::Vigenere;
use crate::traits::{Decrypt, Encrypt};

#[test]
fn encryption_with_valid_input_returns_encrypted_string() {
    let vigenere = Vigenere::new("KEY".to_string());
    let result = vigenere.encrypt("HELLO".to_string());
    assert!(result.is_ok());
    assert_eq!(result.unwrap(), "\u{93}\u{8a}¥\u{97}\u{94}".to_string());
}

#[test]
fn encryption_with_empty_string_returns_empty_string() {
    let vigenere = Vigenere::new("KEY".to_string());
    let result = vigenere.encrypt("".to_string());
    assert!(result.is_ok());
    assert_eq!(result.unwrap(), "".to_string());
}

#[test]
fn encryption_with_non_alphabetic_input_returns_error() {
    let vigenere = Vigenere::new("KEY".to_string());
    let result = vigenere.encrypt("HELLO1".to_string());
    assert!(result.is_ok());
}

#[test]
fn encryption_with_empty_key_returns_error() {
    let vigenere = Vigenere::new("".to_string());
    let result = vigenere.encrypt("HELLO".to_string());
    assert!(result.is_ok());
}

#[test]
fn test_encrypt_decrypt() {
    let vigenere = Vigenere::new("hej".to_string());
    let result = vigenere.encrypt("hello, world!".to_string());
    assert!(result.is_ok());
    let result = vigenere.decrypt(result.unwrap());
    assert!(result.is_ok());
    assert_eq!(result.unwrap(), "hello, world!".to_string());
}