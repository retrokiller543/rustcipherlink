use crate::consts::ALPHABET_LEN;
use crate::crypto_systems::vigenere::error::VigenereError;
use crate::crypto_systems::vigenere::Vigenere;
use crate::traits::{Encode, Encrypt, Decode};

impl Encrypt for Vigenere {
    type Input = String;
    type Output = Result<String, VigenereError>;

    fn encrypt(&self, plain_text: Self::Input) -> <Vigenere as Encrypt>::Output {
        let encoded = plain_text.encode_str()?;
        let encoded_key = self.key.encode_str()?;

        let key_cycle: Vec<_> = encoded_key
            .iter()
            .cycle()
            .take(encoded.len())
            //.copied()
            .collect();

        let encrypted_data = encoded
            .iter()
            .zip(key_cycle.iter())
            .map(|(&x, &y)| (x + y) % ALPHABET_LEN)
            .collect::<Vec<usize>>()
            .decode_str()?;

        Ok(encrypted_data)
    }
}