use std::os::raw::c_uint;
use std::ffi::{CString, CStr};
use std::os::raw::c_char;
use std::ptr;
use crate::crypto_systems::vigenere::Vigenere;
use crate::traits::{Encrypt, Decrypt};

#[repr(C)]
pub struct CVigenere {
    // Since we cannot directly use `String` in C, we use raw pointers.
    key: *mut c_char,
}

impl From<Vigenere> for CVigenere {
    fn from(v: Vigenere) -> Self {
        CVigenere {
            key: CString::new(v.key).unwrap().into_raw()
        }
    }
}

#[no_mangle]
pub extern "C" fn vigenere_new(key: *const c_char) -> *mut CVigenere {
    let key_str = unsafe { CStr::from_ptr(key).to_string_lossy().into_owned() };
    Box::into_raw(Box::new(CVigenere::from(Vigenere::new(key_str))))
}

#[no_mangle]
pub extern "C" fn vigenere_free(c_vigenere: *mut CVigenere) {
    if !c_vigenere.is_null() {
        unsafe {
            let boxed = Box::from_raw(c_vigenere);
            let _ = CString::from_raw(boxed.key); // This drops the CString, freeing the memory
        }
    }
}

#[no_mangle]
pub extern "C" fn vigenere_encrypt(c_vigenere: *const CVigenere, input: *const c_char) -> *mut c_char {
    if c_vigenere.is_null() || input.is_null() {
        return ptr::null_mut();
    }

    let c_vigenere_safe = unsafe { &*c_vigenere };
    let key_str = unsafe { CStr::from_ptr(c_vigenere_safe.key).to_string_lossy().into_owned() };
    let vigenere = Vigenere::new(key_str); // Convert CVigenere to Vigenere

    let cstr_input = unsafe { CStr::from_ptr(input) };

    match cstr_input.to_str() {
        Ok(str_input) => match vigenere.encrypt(str_input.to_string()) {
            Ok(encrypted) => {
                let c_string = CString::new(encrypted).expect("CString::new failed");
                c_string.into_raw()
            },
            Err(_) => ptr::null_mut() // Handle encryption error
        },
        Err(_) => ptr::null_mut() // Handle CStr to_str conversion error
    }
}

#[no_mangle]
pub extern "C" fn vigenere_decrypt(c_vigenere: *const CVigenere, input: *const c_char) -> *mut c_char {
    if c_vigenere.is_null() || input.is_null() {
        return ptr::null_mut();
    }

    let c_vigenere_safe = unsafe { &*c_vigenere };
    let key_str = unsafe { CStr::from_ptr(c_vigenere_safe.key).to_string_lossy().into_owned() };
    let vigenere = Vigenere::new(key_str); // Convert CVigenere to Vigenere

    let cstr_input = unsafe { CStr::from_ptr(input) };

    match cstr_input.to_str() {
        Ok(str_input) => match vigenere.decrypt(str_input.to_string()) {
            Ok(decrypted) => {
                let c_string = CString::new(decrypted).expect("CString::new failed");
                c_string.into_raw()
            },
            Err(_) => ptr::null_mut() // Handle decryption error
        },
        Err(_) => ptr::null_mut() // Handle CStr to_str conversion error
    }
}

// Function to free strings returned from Rust
#[no_mangle]
pub extern "C" fn vigenere_free_string(c_string: *mut c_char) {
    if !c_string.is_null() {
        unsafe { let _ = CString::from_raw(c_string); };
    }
}