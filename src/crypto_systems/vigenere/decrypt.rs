use crate::consts::ALPHABET_LEN;
use crate::crypto_systems::vigenere::error::VigenereError;
use crate::crypto_systems::vigenere::Vigenere;
use crate::traits::{Decode, Decrypt, Encode};

impl Decrypt for Vigenere {
    type Input = String;
    type Output = Result<String, VigenereError>;

    fn decrypt(&self, input: Self::Input) -> Self::Output {
        let encoded = input.encode_str()?;
        let encoded_key = self.key.encode_str()?;

        let key_cycle: Vec<_> = encoded_key
            .iter()
            .cycle()
            .take(encoded.len())
            //.copied()
            .collect();

        let decrypted_data = encoded
            .iter()
            .zip(key_cycle.iter())
            .map(|(&x, &y)| (x + ALPHABET_LEN - y) % ALPHABET_LEN)
            .collect::<Vec<usize>>()
            .decode_str()?;

        Ok(decrypted_data)
    }
    /*
    let encoded = plain_text.encode_str()?;
        let encoded_key = self.key.encode_str()?;

        let key_cycle: Vec<_> = encoded_key
            .iter()
            .cycle()
            .take(encoded.len())
            //.copied()
            .collect();

        let encrypted_data = encoded
            .iter()
            .zip(key_cycle.iter())
            .map(|(&x, &y)| (x + y) % ALPHABET_LEN)
            .collect::<Vec<usize>>()
            .decode_str()?;

        Ok(encrypted_data)
    */
}