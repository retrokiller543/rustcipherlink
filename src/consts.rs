#[cfg(all(feature = "eng", not(feature = "sve"), not(feature = "sve-special")))]
pub const ALPHABET: &str = "abcdefghijklmnopqrstuvwxyz";

#[cfg(all(feature = "eng", not(feature = "sve"), not(feature = "sve-special")))]
pub const ALPHABET_LEN: usize = 26;

#[cfg(all(feature = "sve", not(feature = "eng"), not(feature = "sve-special")))]
pub const ALPHABET: &str = "abcdefghijklmnopqrstuvwxyzåäö";

#[cfg(all(feature = "sve", not(feature = "eng"), not(feature = "sve-special")))]
pub const ALPHABET_LEN: usize = 29;

#[cfg(all(feature = "sve-special", not(feature = "eng"), not(feature = "sve")))]
pub const ALPHABET: &str = "abcdefghijklmnopqrstuvxyzåäö";

#[cfg(all(feature = "sve-special", not(feature = "eng"), not(feature = "sve")))]
pub const ALPHABET_LEN: usize = 28;

#[cfg(all(feature = "utf8", not(any(feature = "sve", feature = "eng", feature = "sve-special"))))]
pub const ALPHABET_LEN: usize = 1112064; // stick to the ascii range minus one to ensure we get a positive number, but we can encode any utf8 character

#[cfg(feature = "parallel")]
pub const THRESHOLD: usize = 5000;