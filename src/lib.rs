#![allow(non_snake_case)]
#![allow(dead_code)]
#![allow(unused_imports)]
#![allow(unused_variables)]
#![allow(unused_mut)]
#![allow(unused_macros)]

pub mod prelude;
pub mod traits;
pub mod crypto_systems;
pub mod error;
pub mod consts;
pub mod utils;
#[cfg(feature = "python-integration")]
mod python_integration;

#[cfg(feature = "python-integration")]
use pyo3::{pyfunction, pymodule, types::PyModule, PyResult, Python};

#[cfg(feature = "python-integration")]
#[pymodule]
fn RustCipherLink(_py: Python<'_>, m: &PyModule) -> PyResult<()> {
    m.add_class::<crypto_systems::ceasar::Ceasar>()?;
    m.add_class::<crypto_systems::vigenere::Vigenere>()?;
    Ok(())
}