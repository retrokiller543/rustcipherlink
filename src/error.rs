use thiserror::Error;
use crate::consts::ALPHABET_LEN;

#[derive(Debug, Error)]
pub enum CharacterError {
    #[error("Invalid character: '{0}'")]
    InvalidCharacter(char),
    #[error("Invalid index: '{0}', max: '{}'", ALPHABET_LEN)]
    InvalidIndex(usize),
}