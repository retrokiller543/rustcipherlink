use pyo3::PyErr;
use crate::error::CharacterError;

impl From<CharacterError> for PyErr {
    fn from(err: CharacterError) -> PyErr {
        match err {
            CharacterError::InvalidCharacter(c) => PyErr::new::<pyo3::exceptions::PyValueError, _>(format!("Invalid character: {}", c)),
            CharacterError::InvalidIndex(idx) => PyErr::new::<pyo3::exceptions::PyValueError, _>(format!("Invalid index: {}", idx)),

        }
    }
}