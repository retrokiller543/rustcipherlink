#!/usr/bin/env just --justfile

# vars
name := "RustCipherLink"
version := "0.0.1"
python_tag := "cp310"
abi_tag := "cp310"
platform_tag := "manylinux_2_34_x86_64"

# default to build both the Rust library and the whl file
default: build build-py-all build-c

# Command to build the Rust library
build:
    @echo "Building the Rust library"
    @cargo build --release

# command to test the Rust library
test:
    @echo "Testing the Rust library"
    @cargo test --features "sequential"

build-py:
    @echo "Building the whl file"
    maturin build --release --features "sequential" --manylinux {{platform_tag}}

# Command to build the whl file
build-py-all:
    @echo "Building the whl files for multiple Python versions"
    ./build-py.sh

# command to install the whl file
install-py: build-py
    @echo "Installing the whl file"
    pip install --force-reinstall target/wheels/{{name}}-{{version}}-{{python_tag}}-{{abi_tag}}-{{platform_tag}}.whl

# command to uninstall the whl file
uninstall-py:
    @echo "Uninstalling the whl file"
    pip uninstall -y target/wheels/{{name}}-{{version}}-{{python_tag}}-{{abi_tag}}-{{platform_tag}}.whl

publish-py: clean-py build-py-all
    @echo "Publishing the whl file"
    @twine upload target/wheels/*.whl

publish-local-py: clean-py build-py-all
    @devpi login root --password ''
    @devpi use root/local
    @devpi upload --formats=bdist_wheel target/wheels/*.whl

# command to publish to crates.io
publish: clean
    @echo "Publishing to crates.io"
    @cargo publish

# command to publish all targets
publish-all: publish publish-py

# command to build the docs
docs:
    @echo "Building the docs"
    @RUSTDOCFLAGS="--html-in-header ./src/docs-header.html" cargo doc --document-private-items --features "sequential" --no-deps --open

# command to clean the build directory (target)
clean:
    @echo "Removing the target directory"
    @cargo clean || true

clean-py:
    @echo "Removing all wheels"
    @rm target/wheels/* || true

cargo-bench-seq:
    @echo "Running the benchmarks"
    @cargo bench --features "sequential"

cargo-bench-par:
    @echo "Running the benchmarks"
    @cargo bench --features "parallel"

# command to display number of lines of code written in Rust
loc:
    @echo "Number of lines of code written in Rust"
    @cloc . --exclude-dir=target --exclude-lang=XML --exclude-dir=examples

build-c-dbg:
    @echo "Building the C library with debug symbols"
    @RUST_BACKTRACE=1 cargo build --features "c-bindings,sequential"
    @cp target/debug/lib{{name}}.so examples/CPP-examples/lib{{name}}.so
    @cp target/debug/lib{{name}}.so examples/C-examples/lib{{name}}.so

build-c:
    @echo "Building the C library"
    @cargo build -r --features "c-bindings,sequential"
    @cp target/release/lib{{name}}.so examples/CPP-examples/lib{{name}}.so
    @cp target/release/lib{{name}}.so examples/C-examples/lib{{name}}.so