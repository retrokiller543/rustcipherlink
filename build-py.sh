#!/bin/bash

# Variables
conda_base=$(conda info --base)  # Get the base path of Conda installation
python_versions=("3.7" "3.8" "3.9" "3.10" "3.11")

# Check if Conda is installed
if [ -z "$conda_base" ]; then
    echo "Conda could not be found. Falling back to simple build."
    maturin build --release
    exit 1
fi

# Function to get the Python interpreter path for a specific Conda environment
get_python_interpreter_path() {
    env_name="py$1"
    env_name=${env_name//./}  # Remove the dot for the environment name
    echo "${conda_base}/envs/${env_name}/bin/python"
}

# Function to build for a specific Python version
build_for_python_version() {
    python_version=$1
    python_path=$(get_python_interpreter_path "$python_version")

    if [ -f "$python_path" ]; then
        echo "Building for Python version $python_version using interpreter $python_path"
        maturin build --release --interpreter "$python_path"
        echo "Build complete for Python version $python_version"
    else
        echo "Python interpreter not found for version $python_version at $python_path"
    fi
}

# Main loop for building
for python_version in "${python_versions[@]}"; do
    build_for_python_version $python_version
done

echo "All builds completed."
