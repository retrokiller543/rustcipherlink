use criterion::{black_box, criterion_group, criterion_main, Criterion};
use RustCipherLink::prelude::*;

fn encode_long_benchmark(c: &mut Criterion) {
    // Generate a string with more than 1000 characters
    let input = "a".repeat(20000);

    c.bench_function("encode_str_long", |b| {
        b.iter(|| {
            let encoded = black_box(&input).encode_str();
            assert!(encoded.is_ok()); // Optional: assert to ensure correctness
        });
    });
}

fn decode_long_benchmark(c: &mut Criterion) {
    // Generate a Vec<usize> with a length similar to the encoded output of the above string
    let input = vec![25; 20000]; // Replace 42 with a representative encoded value

    c.bench_function("decode_str_long", |b| {
        b.iter(|| {
            let decoded = black_box(&input).decode_str();
            assert!(decoded.is_ok()); // Optional: assert to ensure correctness
        });
    });
}

fn encode_short_benchmark(c: &mut Criterion) {
    // Generate a string with more than 1000 characters
    let input = "a".repeat(100);

    c.bench_function("encode_str_short", |b| {
        b.iter(|| {
            let encoded = black_box(&input).encode_str();
            assert!(encoded.is_ok()); // Optional: assert to ensure correctness
        });
    });
}

fn decode_short_benchmark(c: &mut Criterion) {
    // Generate a Vec<usize> with a length similar to the encoded output of the above string
    let input = vec![25; 100]; // Replace 42 with a representative encoded value

    c.bench_function("decode_str_short", |b| {
        b.iter(|| {
            let decoded = black_box(&input).decode_str();
            assert!(decoded.is_ok()); // Optional: assert to ensure correctness
        });
    });
}

criterion_group!(benches, encode_long_benchmark, decode_long_benchmark, encode_short_benchmark, decode_short_benchmark);
criterion_main!(benches);