use criterion::{black_box, criterion_group, criterion_main, Criterion};
use RustCipherLink::crypto_systems::ceasar::Ceasar;
use RustCipherLink::traits::Encrypt;

fn ceasar_encrypt_short_benchmark(c: &mut Criterion) {
    let ceasar = Ceasar { shift: 3 }; // Replace with appropriate initialization
    let input = "ASimpleTestStringxxx".repeat(5); // 20 * 5 = 100 chars
    c.bench_function("ceasar_encrypt_short", |b| {
        b.iter(|| {
            let encrypted = black_box(&ceasar).encrypt(black_box(input.to_string()));
            assert!(encrypted.is_ok()); // Optional: assert to ensure correctness
        });
    });
}

fn ceasar_encrypt_long_benchmark(c: &mut Criterion) {
    let ceasar = Ceasar { shift: 3 }; // Replace with appropriate initialization
    let input = "ASimpleTestStringxxx".repeat(1000); // 20 * 1000 = 20000 chars

    c.bench_function("ceasar_encrypt_long", |b| {
        b.iter(|| {
            let encrypted = black_box(&ceasar).encrypt(black_box(input.to_string()));
            assert!(encrypted.is_ok()); // Optional: assert to ensure correctness
        });
    });
}

criterion_group!(benches, ceasar_encrypt_short_benchmark, ceasar_encrypt_long_benchmark);
criterion_main!(benches);